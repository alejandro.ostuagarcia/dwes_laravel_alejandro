@extends('layouts.master')
@section('titulo')
    Buscador
@endsection
@section("contenido")
    <input  class="input-group" type="text" name="buscar" id="buscar">
<script>
    $(document).ready(function () {
        $("#buscar").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    url: "{{url("pacientes/busquedaAjax")}}",
                    dataType: "json",
                    data: {
                        "_token": "{{csrf_token()}}",
                        "nombre": request['term']
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
        })
    })
</script>
@endsection
