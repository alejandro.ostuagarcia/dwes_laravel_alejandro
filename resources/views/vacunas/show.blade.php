@extends('layouts.master')
@section('titulo')
    Vacuna
@endsection
@section("contenido")
    <h1>{{$vacuna->nombre}}</h1>
    <h2>Pacientes no vacunados:</h2>
    <table>
        <tr>
            <td>Nombre</td>
            <td>Grupo de Vacunacion</td>
            <td>Prioridad</td>
            <td>Accion</td>
        </tr>
        @foreach($vacuna->grupos as $grupo)
            @foreach($grupo->pacientes as $paciente)
                @if($paciente->vacunado==false)
                    <tr>
                        <td>{{$paciente->nombre}}</td>
                        <td>{{$grupo->nombre}}</td>
                        <td>{{$grupo->prioridad}}</td>
                        <td>
                            <form action="{{route("vacunar",$paciente)}}" method="post">
                                @csrf
                                <input type="button" value="Vacunar" name="" id="">
                            </form>
                        </td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    </table>
<h2>Pacientes Vacunados</h2>
    <table>
        <tr>
            <td>Nombre</td>
            <td>Grupo de Vacunacion</td>
            <td>Prioridad</td>
            <td>Accion</td>
        </tr>
        @foreach($vacuna->grupos as $grupo)
            @foreach($grupo->pacientes as $paciente)
                @if($paciente->vacunado==true)
                    <tr>
                        <td>{{$paciente->nombre}}</td>
                        <td>{{$grupo->nombre}}</td>
                        <td>{{$grupo->prioridad}}</td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    </table>
            @endsection
