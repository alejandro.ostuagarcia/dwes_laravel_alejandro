@extends('layouts.master')
@section('titulo')
    Vacunas
@endsection
@section("contenido")
    @foreach($vacunas as $vacuna)
        <div class="border shadow ">
            <a class="text-decoration-none text-dark" href="{{route("vacunas.show",$vacuna)}}">
            <h1>{{$vacuna->nombre}}</h1>
            <p>Posibles grupos de vacunacion: </p>
            <ul>
                @foreach($vacuna->grupos as $grupo)
                    <li>{{$grupo->nombre}}</li>
                @endforeach
            </ul>
            </a>
        </div>

    @endforeach
@endsection
