<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VacunaController;
use App\Http\Controllers\PacienteController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("/",[VacunaController::class,"home"]);
//Route::get("/",[VacunaController::class])->name("home");
Route::get("/vacunas",[VacunaController::class,"index"])->name("vacunas.index");
Route::get("/vacunas/{vacuna}",[VacunaController::class,"show"])->name("vacunas.show");

Route::post("/pacientes/{paciente}/vacunar",[PacienteController::class])->name("vacunar");

//REst
Route::post("/api/vacunas/crear",[VacunaController::class,'create']);
Route::get("/api//vacunas/{idPaciente}",[VacunaController::class,"posibles"]);
//ajax
Route::get("/pacientes/buscador",[PacienteController::class,"buscar"])->name("pacientes.buscador");
Route::post('pacientes/busquedaAjax', [PacienteController::class,'buscador']);
