<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use HasFactory;
    protected $table="pacientes";
    public function getRouteKeyName() {
        return'slug';
    }
    public function grupos(){
        return $this->hasMany(Grupo::class);
    }
}
