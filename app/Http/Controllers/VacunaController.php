<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VacunaController extends Controller
{
    public function home(){
        return  redirect()->route('vacunas.index');
    }
    public function index(){
        $vacunas = Vacuna::all();
        return view("vacunas.index",compact("vacunas"));
    }
    public function show(Vacuna $vacuna){
        return view("vacunas.show",["vacuna"=>$vacuna]);
    }
    public function create(Vacuna $vacuna){
        $v = new Vacuna();
        $v->nombre = $vacuna[0];
        $v->slug = Str::slug($vacuna[0]);
        $v->save();
        return response()->json(["mensaje"=>"La vacuna insertada correctamente"]);
    }
    public function vacunar(Paciente $paciente){
        $p = Paciente::findOrFail($paciente);
        $p->vacunado = true;
        $p->save();
    }
}
