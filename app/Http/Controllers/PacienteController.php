<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    public function buscar(){
        return view("pacientes.buscador");
    }
    public function buscador(Request $request){
        $p = Paciente::where('nombre','like',"%$request->nombre%")->pluck("nombre");
        return response()->json($p);
    }
}
