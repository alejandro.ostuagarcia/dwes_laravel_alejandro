<?php

namespace Database\Factories;

use App\Models\Grupo;
use App\Models\paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class pacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name;
        $vacunado = $this->faker->boolean;
//        if ($vacunado==true){
        $fecha = $this->faker->dateTimeThisYear;
//        }else $fecha = null;
        return [
            "nombre"=>$nombre,
            "slug"=>Str::slug($nombre),
            "vacunado"=>$vacunado,
            "fechaVacuna"=>$fecha,
            "grupo_id"=>Grupo::all()->random()->id,
        ];
    }
}
